

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;


@WebServlet("/AutoCompletInputServlet")
public class AutoCompletInputSertvlet extends HttpServlet {
	
	//This array will has all the words 
	public static ArrayList<String> words = new ArrayList<String>();
	//This array will show the suggestions of words in JSON format.
	public static JSONArray arrayObj = new JSONArray();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		//calling insertWords method to add all the words to the ArrayList
		insertWords();
		
		
		PrintWriter pw = response.getWriter();
		
		//Getting the value has been wrote in the input
		String inputValue = request.getParameter("term");
		
		//Returning the response value in JSON format
		response.setContentType("application/json");
	
		//returning response of the request
		pw.print(getWords(inputValue));
		
		
		
		
		
	}
	
	public static void insertWords() {
		words.add("Kayak, Pinterest, Paypal, Pg&e, Project free tv, Priceline, Press democrat, Progressive, Project runway,\n" +
                "Proactive, Programming, Progeria, Progesterone, Progenex, Procurable, Processor, Proud, Print, Prank,\n" +
                "Bowl, Owl, River, Phone, Pandora, Stamps, Reprobe");
	}
	
	//This method will return all the words of ArrayList words
	public static JSONArray getWords(String inputValue){
		
		//this array store words separated by comma and space
		String separatedWords [];
		
		for (int i = 0; i < words.size();i++){
            String word = words.get(i); 
          
            separatedWords = word.split("\\s*,\\s*");//Split words by comma and space
            for (int x = 0;x < separatedWords.length;x++){ 
                  Arrays.sort(separatedWords);//Sorting the array alphabetically to show in the input in order
            	
                  if (separatedWords[x].toLowerCase().contains(inputValue)){//Checking if the array contain the value has wrote in input
                     	
                	  /*Checking if words has been added before in the array, if isn't, then will add.
                     	 * Because if a word is already in the array, and the same word add again, then it will show more
                     	 * than once in the suggestions list
                     	 * */
                	if(!arrayObj.toString().contains(separatedWords[x])) {
                		//Adding the words that will show in input suggestions
                		arrayObj.add(separatedWords[x]);
                	
            		}
             	
                      
               }
            }

        }
		
		
		
		return arrayObj;
		
	}
	
	
	
	
	

	

}
